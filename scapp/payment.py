import hashlib


class PaymentDoer:
    def __init__(self, chain_length=10):
        self.chain_length = chain_length
        self.chain = []

        self.secret = 'dayum, you got ma\' secret'

    def generate_chain(self):
        my_hasher = hashlib.sha1()
        chain = []
        chain.append(self.secret)

        i = 0
        while i < self.chain_length:
            my_hasher.update(str(chain[-1]).encode('utf-8'))
            chain.append(my_hasher.hexdigest())
            i += 1

        self.chain = chain
        return chain


if __name__ == '__main__':
    pd = PaymentDoer()
    pd.generate_chain()

    print(pd.chain)



