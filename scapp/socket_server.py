import socket
import threading


class BaseSocketServer:
    def __init__(self, host=None, port=None):

        if not host or not port:
            raise Exception('No host or port given')

        self.host = host
        self.port = port

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.bind((self.host, self.port))

    def start_server(self, thread_method=None, after_start_method=None):

        if thread_method is None:
            raise Exception('No thread_method found')

        self.server_socket.listen(5)

        if after_start_method is not None:
            after_start_method()

        while True:
            client, address = self.server_socket.accept()
            client.settimeout(60)
            threading.Thread(
                target=thread_method,
                args=(client, address),
            ).start()
