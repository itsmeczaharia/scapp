from scapp.socket_server import BaseSocketServer
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA
import config
from time import sleep
import socket


class Vendor(BaseSocketServer):
    def __init__(self, vendor_identity='DefaultVendorIdentity', host=None, port=None):
        super().__init__(host=host, port=port)
        self.identity = vendor_identity
        self.commit_certificate = ''
        self.user_key = ''
        self.user_info = ''
        self.user_key_rsa = ''

        self.broker_key = ''
        self.broker_certificate = ''
        self.broker_plain = ''

        self.transaction_active = False
        self.broker_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.broker_socket.connect((config.BROKER_HOST, config.BROKER_PORT))
        print('Connected to Broker')
        self.start_server(thread_method=self.vendor_manager, after_start_method=self.after_start)

    def vendor_manager(self, client, address):
        print('Client Connected to %s' % self.identity)
        while True:
            try:
                cmd = client.recv(10).decode()
                print(cmd)
                if cmd == 'START':
                    client.send(self.identity.encode('utf-8'))
                    print('Identity Sent %s' % self.identity)
                    self.sig_flag = True

                if cmd == 'COMMIT':
                    self.commit_certificate = client.recv(1000)
                    print('Recieved commited_certificate')
                    sleep(1)
                    self.user_key = client.recv(1000)
                    print('Recieved user key')
                    sleep(1)
                    self.user_info = client.recv(1000)

                    h = SHA.new(self.user_info)
                    self.user_key_rsa = RSA.importKey(self.user_key)
                    verifier = PKCS1_v1_5.new(self.user_key_rsa)

                    if verifier.verify(h, self.commit_certificate):
                        print('%s signature OK!' % self.identity)
                        """
                            Here, all info from user are ok, let'c check the broker
                        """
                        self.broker_socket.send(b'GIVEME')
                        sleep(1)
                        self.broker_key = self.broker_socket.recv(1000)
                        sleep(1)
                        self.broker_certificate = self.broker_socket.recv(1000)
                        sleep(1)
                        self.broker_plain = self.broker_socket.recv(1000)
                        sleep(1)

                        h = SHA.new(self.broker_plain)
                        self.user_key_rsa = RSA.importKey(self.broker_key)
                        verifier = PKCS1_v1_5.new(self.user_key_rsa)

                        if verifier.verify(h, self.broker_certificate):
                            print('Broker signature OK!')
                            self.transaction_active = True


            except Exception as e:
                print(e)
                client.close()
                return False

    def after_start(self):
        print('Server Started %s' % self.identity)


if __name__ == '__main__':
    vendor = Vendor(host=config.VENDOR_HOST, port=config.VENDOR_PORT)


