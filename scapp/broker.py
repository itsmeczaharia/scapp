from scapp.socket_server import BaseSocketServer
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA
from Crypto import Random

from json import JSONEncoder
from time import sleep
import config


class Broker(BaseSocketServer):
    def __init__(self, broker_identity='Broker01', host=None, port=None):
        super().__init__(host=host, port=port)
        self.identity = broker_identity

        random_generator = Random.new().read
        self.key = RSA.generate(1024, random_generator)
        self.public_key = self.key.publickey()

        self.user_info = ''
        self.certificate = None
        self.start_server(thread_method=self.broker_manager, after_start_method=self.after_start)

        self.sig_flag = False

    def broker_manager(self, client, address):
        print('Client Connected to %s' % self.identity)
        while True:
            try:
                cmd = client.recv(10).decode()
                if cmd == 'START':
                    self.user_info = client.recv(1000)

                    signer = PKCS1_v1_5.new(self.key)
                    self.certificate = signer.sign(SHA.new(self.user_info))
                    client.send(self.public_key.exportKey())
                    sleep(1)
                    client.send(self.certificate)
                    reply = client.recv(10).decode()

                    if reply == '1':
                        print('Signature OK')
                        self.sig_flag = True
                    else:
                        print('Signature nOK')
                        client.close()

                if cmd == 'GIVEME':
                    client.send(self.public_key.exportKey())
                    sleep(1)
                    client.send(self.certificate)
                    sleep(1)
                    client.send(self.user_info)
                    sleep(1)

                if self.sig_flag is True:
                    # cmd = client.recv(10).decode()
                    pass
                else:
                    raise Exception('Client disconnected')
            except:
                client.close()
                return False

    def after_start(self):
        print('Server Started %s' % self.identity)


if __name__ == '__main__':
    broker = Broker(host=config.BROKER_HOST, port=config.BROKER_PORT)
