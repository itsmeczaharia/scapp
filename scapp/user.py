from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA
from Crypto import Random

from scapp.payment import PaymentDoer

from json import JSONEncoder
from time import sleep
import json

import socket
import config


class User:
    def __init__(self, user_identity='User01'):
        random_generator = Random.new().read
        self.key = RSA.generate(1024, random_generator)
        self.public_key = self.key.publickey()

        self.identity = user_identity
        self.cardNumber = '18232135'
        self.expDate = '12-12-19'

        self.broker_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.broker_socket.connect((config.BROKER_HOST, config.BROKER_PORT))
        print('Connected to Broker')

        self.vendor_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.vendor_socket.connect((config.VENDOR_HOST, config.VENDOR_PORT))
        print('Connected to Vendor')

        self.broker_phase()
        self.payment_doer = PaymentDoer()
        self.payment_chain = self.payment_doer.generate_chain()
        print('Payment Created!\n')

        self.vendor_identity = self.vendor_phase()
        print('Vendor\'s Identity recieved, %s' % self.vendor_identity)

        self.commit_certificate = self.commit_and_send()
        print(self.commit_certificate)

    def vendor_phase(self):
        self.vendor_socket.send(b'START')
        sleep(1)
        return self.vendor_socket.recv(1000).decode()

    def broker_phase(self):
        self.broker_socket.send(b'START')
        sleep(1)
        temp_dict = {"info": [self.identity, self.cardNumber, self.expDate, self.public_key.exportKey().decode()]}
        json_string = JSONEncoder().encode(temp_dict)
        self.broker_socket.send(str.encode(json_string))
        self.brokerKeyString = self.broker_socket.recv(1000)
        self.certificate = self.broker_socket.recv(1000)
        self.brokerKey = RSA.importKey(self.brokerKeyString)
        h = SHA.new(str.encode(json_string))
        verifier = PKCS1_v1_5.new(self.brokerKey)

        if verifier.verify(h, self.certificate):
            print('%s signature OK!' % self.identity)
            self.broker_socket.send(b'1')
        else:
            print('%s invalid signature!' % self.identity)
            self.broker_socket.send(b'0')
            self.broker_socket.close()

    def commit_and_send(self):
        self.vendor_socket.send(b'COMMIT')
        sleep(1)

        temp_dict = {"info": [self.vendor_identity, self.certificate.decode('utf-8', 'ignore'),
                              self.payment_chain[-1], self.payment_doer.chain_length]}

        json_string = json.dumps(temp_dict).encode('utf-8')
        signer = PKCS1_v1_5.new(self.key)
        certificate = signer.sign(SHA.new(json_string))

        self.vendor_socket.send(certificate)
        sleep(1)
        self.vendor_socket.send(self.public_key.exportKey())
        sleep(1)
        self.vendor_socket.send(json.dumps(temp_dict).encode())
        return certificate


    # def do_transaction(self):
    #     if len(self.payment_chain) >

if __name__ == '__main__':
    user = User()
